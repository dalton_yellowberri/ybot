const Discord = require('discord.js');
const client = new Discord.Client();

const config = require('./config.json');

/* ------------------------------------------------------------------------ */
/* On Ready and Login / Logout */
/* ------------------------------------------------------------------------ */

client.on('ready', () => {
	console.log('Bot has started, with ' + client.users.size + ' users, in ' + client.channels.size + ' channels of ' + client.guilds.size + ' servers.');
	client.user.setGame('on ' + client.guilds.size + ' servers');
});

client.on('guildCreate', guild => {

	console.log('New guild joined: ' + guild.name + ' (id: ' + guild.id + '). This guild has ' + guild.memberCount + ' members!');
	client.user.setGame('on ' + client.guilds.size + ' servers');
});

client.on('guildDelete', guild => {
	console.log('I have been removed from: ' + guild.name + ' (id: ' + guild.id + ')');
	client.user.setGame('on ' + client.guilds.size + ' servers');
});

/* ------------------------------------------------------------------------ */
/* On Discord Message */
/* ------------------------------------------------------------------------ */

module.exports.onMessage = function onMessage( message ) {
	console.log( message );
}

client.on('message', async message => {
	var command = '';
	module.exports.onMessage( message.content );

	if ( message.author.bot ) {
		return;
	}
	if ( message.content.indexOf(config.command_prefix ) !== 0 ) {
		return;
	}
	else {
		var args = message.content.slice( config.command_prefix.length ).trim().split(/ +/g);
		command = args.shift().toLowerCase();
		console.log( command );
		console.log( args );
	}

	if ( command === 'ping' ) {
		const m = await message.channel.send('Ping?');
		m.edit('Pong! Latency is ' + ( m.createdTimestamp - message.createdTimestamp ) + 'ms. API Latency is ' + Math.round(client.ping) + 'ms');
	}
	if ( command === 'say' ) {
		const sayMessage = args.join(' ');
		message.delete().catch(O_o=>{});
		message.channel.send(sayMessage);
	}
});

module.exports.start = function( token ) {
	client.login( token );
}
