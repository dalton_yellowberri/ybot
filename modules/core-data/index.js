var path = require('path');
var fs = require('fs');

var Data = {};
Data.stores = {};

/*
Important Containers:
core - Main app settings
*/

/* ------------------------------------------------------------------------ */
/* Set data in the store */
/* ------------------------------------------------------------------------ */

Data.set = function( container, prop, value ) {
	if ( typeof this.stores[container] === 'undefined' ) {
		this.stores[container] = {};
	}

	this.stores[container][prop] = value;
	return this.stores[container];
}

/* ------------------------------------------------------------------------ */
/* Get data from the store */
/* ------------------------------------------------------------------------ */

Data.get = function( container, prop ) {
	return this.stores[container][prop];
}

/* ------------------------------------------------------------------------ */
/* Update data in the store, and return updated container */
/* ------------------------------------------------------------------------ */

Data.update = function( container, prop, value ) {
	if ( typeof this.stores[container] === 'undefined' ) {
		console.log( 'Set store properties before updating them?' );
	}

	this.stores[container].prop = value;
	return this.stores[container];
}

/* ------------------------------------------------------------------------ */
/* Load Core Modules */
/* ------------------------------------------------------------------------ */

Data.loadCoreModules = function( path ) {
	var paths = [];

	fs.readdirSync( path ).forEach( function( file ) {
		paths.push( file );
	});

	this.set('core', 'modulesPath', paths);
}

module.exports = Data;
