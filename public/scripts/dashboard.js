(function($) {

	var hype = {
		codeOne: '',
		codeOneTotal : 0,
		codeOneCurrent : 0,
		codeOneRatio : 0,
		codeTwo: '',
		codeTwoTotal : 0,
		codeTwoCurrent : 0,
		codeTwoRatio : 0,
		max : 50,
		decay : 0.1,
		interval : 1000
	}

	var hypeLoop;

	function updateHypeDiv( ratio, selector ) {
		console.log( ratio );

		function updateHypeSelector( level, selector ) {
			if ( $('.hype.' + selector ).hasClass('hype-level-' + level ) ) {
				return;
			}
			else {
				$('.hype.' + selector ).removeClass(function (index, css) {
					return (css.match (/\bhype-level-\S+/g) || []).join(' ');
				});
				$('.hype.' + selector ).addClass('hype-level-' + level )
			}

		}

		if ( ratio >= 1) {
			updateHypeSelector('max', selector)
		}
		else if ( ratio >= .8 && ratio < 1) {
			updateHypeSelector('5', selector)
		}
		else if ( ratio >= .6 && ratio < .8) {
			updateHypeSelector('4', selector)
		}
		else if ( ratio >= .4 && ratio < .6) {
			updateHypeSelector('3', selector)
		}
		else if ( ratio >= .2 && ratio < .4) {
			updateHypeSelector('2', selector)
		}
		else if ( ratio >= .2 && ratio < .4) {
			updateHypeSelector('1', selector)
		}
		else if ( ratio < .2 ) {
			updateHypeSelector('0', selector)
		}
	}

	socket.on('hype_set', function( data ) {
		clearInterval( hypeLoop )
		console.log( data );

		hypeLoop = setInterval( function() {

			if ( hype.codeOneCurrent > 1 ) {
				hype.codeOneRatio = hype.codeOneCurrent / hype.max;
				hype.codeOneRatio = hype.codeOneRatio.toFixed(2);

				updateHypeDiv( hype.codeOneRatio, 'one' );
				hype.codeOneCurrent = hype.codeOneCurrent * ( 1 - hype.decay );
			}

			if ( hype.codeTwoCurrent > 1 ) {
				hype.codeTwoRatio = hype.codeTwoCurrent / hype.max;
				hype.codeTwoRatio = hype.codeTwoRatio.toFixed(2);

				updateHypeDiv( hype.codeTwoRatio, 'two' )
				hype.codeTwoCurrent = hype.codeTwoCurrent * ( 1 - hype.decay );
			}

			$('.hype-one-current').text('Hype Ratio: ' + hype.codeOneRatio );
			$('.hype-two-current').text('Hype: Ratio: ' + hype.codeTwoRatio );

		}, hype.interval )
	});

	socket.on('hype_up_one', function() {

		hype.codeOneTotal = hype.codeOneTotal + 1;
		hype.codeOneCurrent = hype.codeOneCurrent + 10;
		$('.hype-one-total').text('Total Hype: ' + hype.codeOneTotal );
	})

	socket.on('hype_up_two', function() {

		hype.codeTwoTotal = hype.codeTwoTotal + 1;
		hype.codeTwoCurrent = hype.codeTwoCurrent + 10;
		$('.hype-two-total').text('Total Hype: ' + hype.codeTwoTotal );
	})

	function swapTeams() {
		var temp = {};
		var inputs = ['name', 'square', 'code', 'color', 'score'];

		/* Load input values into temp obj */
		$.each( inputs, function(index, val) {
			temp[index] = $('*[name=team1_' + val + ']').val();
		});

		/* Replace team1 values with team2 values */
		$.each( inputs, function(index, val) {
			$('*[name=team1_' + val + ']').val( $('*[name=team2_' + val + ']').val() );
		});

		/* Replace team2 values with temp obj value */
		$.each( inputs, function(index, val) {
			$('*[name=team2_' + val + ']').val( temp[index] );
		});
	}

	function extendElemData(elem, obj) {
		var ext = $( elem ).data('extends');


		/* If the element has data-extends, split args into array */
		if ( typeof ext !== 'undefined' ) {
			var extArray = [];

			if ( ext.indexOf(',') !== -1 ) {
				extArray = ext.split(',');
			}
			else {
				extArray[0] = ext;
			}

			/* Grab the value of name=arg and add it to obj */
			for (var i = 0; i < extArray.length; i++) {
				var key = extArray[i];
				obj[key] = $( '*[name=' + extArray[i] + ']' ).get(0).value
			}
		}
	}

	/* ------------------------------------------------------------------------ */
	/* Capture all links as no option commands */
	/* ------------------------------------------------------------------------ */

	$('a').on('click', function( event ) {

		/* Add secret to our request */
		var postData = { 'secret' : secret };
		event.preventDefault();

		/* If is reverse, do that */
		if ( $( this ).data('action') === '/update/team_reverse' ) {
			swapTeams();
			return;
		}

		/* Update dashboard on score increases */
		if ( $( this ).data('action') === '/update/team1_up' ) {
			$('input[name=team1_score').val( parseInt( $('input[name=team1_score').val() ) + 1 );
		}

		if ( $( this ).data('action') === '/update/team2_up' ) {
			$('input[name=team2_score').val( parseInt( $('input[name=team2_score').val() ) + 1 );
		}

		/* Add extra data to request object */
		extendElemData( this, postData );

		/* Send POST request to node server */
		$.ajax({
			type: "POST",
			url: $( this ).data('action'),
			data: postData,
			success: function(data) {
			},
			dataType: 'JSON'
		});
	});

	/* ------------------------------------------------------------------------ */
	/* Capture all forms and submit data */
	/* ------------------------------------------------------------------------ */

	$('form').submit( function( event ) {

		/* Prevent default and serialize data */
		event.preventDefault();
		var formData = $( this ).serializeArray();

		/* Turn our indexed array into key value pairs */
		var formJSON = {};
		$( formData ).each( function( index ) {
			formJSON[formData[index].name] = formData[index].value;
		});
		formJSON['secret'] = secret;

		extendElemData( this, formJSON );

		/* Send the formatted data to the server */
		$.ajax({
			type: "POST",
			url: $( this ).attr('action'),
			data: formJSON,
			success: function(data) {
			},
			dataType: 'JSON'
		});
	});

	/* ------------------------------------------------------------------------ */
	/* Expand .groups on click */
	/* ------------------------------------------------------------------------ */

	$('.group h1').on('click', function( event ) {
		/* Toggle hide / show for input groups */
		$( this ).parent('.group').toggleClass('open');
	});

})(jQuery);
