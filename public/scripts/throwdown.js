(function($) {
  socket.on("refresh", function(data) {
    location.reload();
  });

  socket.on("hype_set", function(data) {
    console.log(data);
  });

  /* ------------------------------------------------------------------------ */
  /* General Messages and Pop Overs */
  /* ------------------------------------------------------------------------ */

  socket.on("caster_msg", function(data) {
    console.log(data);
    $("#caster-msg").toggleClass("open");
    $("#caster1 .mini").text(data.castermini1);
    $("#caster1 .name").text(data.caster1);
    $("#caster1 .twitter").text(data.twitter1);
    $("#caster2 .mini").text(data.castermini2);
    $("#caster2 .name").text(data.caster2);
    $("#caster2 .twitter").text(data.twitter2);
  });

  socket.on("general_msg", function(data) {
    console.log(data);

    if ($("#general-popover").hasClass("open")) {
      $("#general-popover").toggleClass("open");
    } else {
      $("#general-popover .title").text(data.general_title);
      $("#general-popover .text").text(data.general_text);
      $("#general-popover .text")
        .text()
        .replace(/\↵/g, "<br />");
      $("#general-popover").toggleClass("open");
    }
  });

  /* ------------------------------------------------------------------------ */
  /* Middle Text */
  /* ------------------------------------------------------------------------ */

  socket.on("middle_text", function(data) {
    $("#middle-text span").css("opacity", 0);
    $("#middle-text.hide").removeClass("hide");

    setTimeout(function() {
      $("#middle-text span").text(data.text);
      $("#middle-text span").css("opacity", 1);
    }, 1000);
  });

  socket.on("middle_hide", function(data) {
    $("#middle-text").toggleClass("hide");
  });

  /* ------------------------------------------------------------------------ */
  /* Scoreboard */
  /* ------------------------------------------------------------------------ */

  function teamUp(team, int) {
    var score = parseInt($("#" + team + "_scoreboard").text()) + 1;
    var teamSelector = "." + team;

    $("." + team + "_score").text(score);

    $(teamSelector + " .team-score").addClass("onchange");
    $(teamSelector + " .team-name").css(
      "background-color",
      "rgba(255,255,255,0.7"
    );
    $(teamSelector + " .team-name .name").css("color", "#fbae17");

    setTimeout(function() {
      $(teamSelector + " .team-score").removeClass("onchange");
      $(teamSelector + " .team-name").css("background-color", "");
      $(teamSelector + " .team-name .name").css("color", "");
    }, 2000);
  }

  socket.on("team1_up", function(data) {
    console.log(data);
    teamUp("team1", 1);
  });

  socket.on("team2_up", function(data) {
    console.log(data);
    teamUp("team2", 1);
  });

  socket.on("team_set", function(data) {
    console.log(data);

    $(".team1_name").text(data.team1_name);
    $(".team1_code").attr("src", "/images/" + data.team1_code + ".png");
    $(".team1_score").text(data.team1_score);
    $(".team1_square").attr(
      "src",
      "/images/square-" + data.team1_square + ".png"
    );
    $(".team1 .code-under, .team1 .code-emblem").css("fill", data.team1_color);
    $("#team1_versus .versus_bg").css("background-color", data.team1_color);

    $(".team2_name").text(data.team2_name);
    $(".team2_code").attr("src", "/images/" + data.team2_code + ".png");
    $(".team2_score").text(data.team2_score);
    $(".team2_square").attr(
      "src",
      "/images/square-" + data.team2_square + ".png"
    );
    $(".team2 .code-under, .team2 .code-emblem").css("fill", data.team2_color);
    $("#team2_versus .versus_bg").css("background-color", data.team2_color);
  });

  socket.on("team_reverse", function(data) {
    console.log(data);
    $("#viewport").toggleClass("reverse_teams");
  });

  /* ------------------------------------------------------------------------ */
  /* Cards */
  /* ------------------------------------------------------------------------ */

  function hideCards() {
    $("#cards .active").removeClass("active");
  }

  socket.on("card_text", function(data) {
    console.log(data);

    hideCards();

    setTimeout(function() {
      if (typeof data.bg !== "undefined") {
        $("#cards #card-bg").addClass("active");

        var bgVideo = "";

        if (data.bg === "rand") {
          var allBg = $("#card-bg video");
          var rand = Math.floor(Math.random() * allBg.length);
          bgVideo = allBg.get(rand);
        } else {
          bgVideo = $("video." + data.bg).get(0);
        }

        $(bgVideo).addClass("active");
        bgVideo.play();
      }

      $("#cards #card-text").addClass("active");
      $("#card-text .card-text").text(data.text);

      if (data.more_text === "") {
        $("#card-text .more-text").addClass("hide");
      } else {
        $("#card-text .more-text").text(data.more_text);
        $("#card-text .more-text.hide").removeClass("hide");
      }
    }, 1000);
  });

  socket.on("card_versus", function(data) {
    hideCards();

    $("video.versus").addClass("active");
    $("#cards #card-bg").addClass("active");

    setTimeout(function() {
      $("#cards #card-versus").addClass("active");
    }, 1000);
  });

  socket.on("card_hide", function(data) {
    setTimeout(function() {
      hideCards();
    }, 1000);
  });

  /* ------------------------------------------------------------------------ */
  /* Bug */
  /* ------------------------------------------------------------------------ */

  function resetBug() {
    $("#bug.hide").removeClass("hide");
    $("#bug-inner .active").removeClass("active");
  }

  socket.on("bug_embiggen", function(data) {
    $("#bug").toggleClass("embiggen");
  });

  socket.on("bug_text", function(data) {
    resetBug();

    setTimeout(function() {
      $("#bug-inner .bug_text").text(data.text);
      $("#bug-inner #bug-text").addClass("active");
    }, 1000);
  });

  socket.on("bug_logo", function(data) {
    resetBug();

    setTimeout(function() {
      $("#bug-inner #bug-logo").addClass("active");
    }, 1000);
  });

  socket.on("bug_countdown", function(data) {
    resetBug();

    setTimeout(function() {
      $("#bug-inner #bug-countdown").addClass("active");
    }, 1000);
  });

  socket.on("bug_hide", function(data) {
    $("#bug").toggleClass("hide");
  });

  /* ------------------------------------------------------------------------ */
  /* Countdown */
  /* ------------------------------------------------------------------------ */

  var countDownInterval;
  var countDownDate;

  function addHours(date, hr) {
    return new Date(date.getTime() + hr * 3600 * 1000);
  }
  function addMinutes(date, min) {
    return new Date(date.getTime() + min * 60000);
  }
  function addSeconds(date, sec) {
    return new Date(date.getTime() + sec * 1000);
  }

  function countDownComplete() {
    resetBug();
    clearInterval(countDownInterval);

    setTimeout(function() {
      if ($("#bug").hasClass("embiggen")) {
        $("#bug").removeClass("embiggen");
      }

      $("#bug-inner #bug-logo").addClass("active");
    }, 1000);
  }

  function setCountdown(date) {
    var tempCount = date.getTime();
    countDownInterval = setInterval(function() {
      var now = new Date().getTime();
      var distance = countDownDate - now;

      var days = Math.floor(distance / (1000 * 60 * 60 * 24));
      var hours = Math.floor(
        (distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60)
      );
      var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
      var seconds = Math.floor((distance % (1000 * 60)) / 1000);

      if (hours < 0) {
        hours = 0;
      }
      if (minutes < 0) {
        minutes = 0;
      }
      if (seconds < 0) {
        seconds = 0;
      }

      if (minutes < 10) {
        minutes = "0" + minutes.toString();
      }
      if (seconds < 10) {
        seconds = "0" + seconds.toString();
      }

      document.getElementById("hours").innerHTML = hours;
      document.getElementById("minutes").innerHTML = minutes;
      document.getElementById("seconds").innerHTML = seconds;

      if (distance <= 0) {
        countDownComplete();
      }
    }, 1000);
  }

  socket.on("countdown", function(data) {
    console.log(data);
    resetBug();

    var hr = parseInt(data.hours) || 0;
    var min = parseInt(data.minutes) || 0;
    var sec = parseInt(data.seconds) || 0;

    countDownDate = new Date();
    countDownDate = addHours(countDownDate, hr);
    countDownDate = addMinutes(countDownDate, min);
    countDownDate = addSeconds(countDownDate, sec + 2);
    setCountdown(countDownDate);

    setTimeout(function() {
      if ($("#bug").hasClass("embiggen") === false) {
        $("#bug").addClass("embiggen");
      }

      $("#bug-inner #bug-countdown").addClass("active");
    }, 1000);
  });

  /* ------------------------------------------------------------------------ */
  /* Hype Meters */
  /* ------------------------------------------------------------------------ */

  var hype = {
    codeOne: "",
    codeOneTotal: 0,
    codeOneCurrent: 0,
    codeOneRatio: 0,
    codeTwo: "",
    codeTwoTotal: 0,
    codeTwoCurrent: 0,
    codeTwoRatio: 0,
    max: 50,
    decay: 0.1,
    interval: 1000
  };

  var hypeLoop;

  function updateHypeDiv(ratio, selector) {
    console.log(ratio);
    var perc = ratio * 100 + "%";
    $(".hype.hype-meter." + selector + " .bar").css("width", perc);

    function updateHypeSelector(level, selector) {
      if ($(".hype." + selector).hasClass("hype-level-" + level)) {
        return;
      } else {
        $(".hype." + selector).removeClass(function(index, css) {
          return (css.match(/\bhype-level-\S+/g) || []).join(" ");
        });
        $(".hype." + selector).addClass("hype-level-" + level);
      }
    }

    if (ratio >= 1) {
      updateHypeSelector("max", selector);
    } else if (ratio >= 0.8 && ratio < 1) {
      updateHypeSelector("5", selector);
    } else if (ratio >= 0.6 && ratio < 0.8) {
      updateHypeSelector("4", selector);
    } else if (ratio >= 0.4 && ratio < 0.6) {
      updateHypeSelector("3", selector);
    } else if (ratio >= 0.2 && ratio < 0.4) {
      updateHypeSelector("2", selector);
    } else if (ratio >= 0.2 && ratio < 0.4) {
      updateHypeSelector("1", selector);
    } else if (ratio < 0.2) {
      updateHypeSelector("0", selector);
    }
  }

  socket.on("hype_set", function(data) {
    clearInterval(hypeLoop);
    console.log(data);

    hypeLoop = setInterval(function() {
      if (hype.codeOneCurrent > 1) {
        hype.codeOneRatio = hype.codeOneCurrent / hype.max;
        hype.codeOneRatio = hype.codeOneRatio.toFixed(2);

        updateHypeDiv(hype.codeOneRatio, "one");
        hype.codeOneCurrent = hype.codeOneCurrent * (1 - hype.decay);
      }

      if (hype.codeTwoCurrent > 1) {
        hype.codeTwoRatio = hype.codeTwoCurrent / hype.max;
        hype.codeTwoRatio = hype.codeTwoRatio.toFixed(2);

        updateHypeDiv(hype.codeTwoRatio, "two");
        hype.codeTwoCurrent = hype.codeTwoCurrent * (1 - hype.decay);
      }

      $(".hype-one-current").text("Hype Ratio: " + hype.codeOneRatio);
      $(".hype-two-current").text("Hype: Ratio: " + hype.codeTwoRatio);
    }, hype.interval);
  });

  socket.on("hype_up_one", function() {
    hype.codeOneTotal = hype.codeOneTotal + 1;
    hype.codeOneCurrent = hype.codeOneCurrent + 10;
    $(".hype-one-total").text("Total Hype: " + hype.codeOneTotal);
  });

  socket.on("hype_up_two", function() {
    hype.codeTwoTotal = hype.codeTwoTotal + 1;
    hype.codeTwoCurrent = hype.codeTwoCurrent + 10;
    $(".hype-two-total").text("Total Hype: " + hype.codeTwoTotal);
  });
})(jQuery);
