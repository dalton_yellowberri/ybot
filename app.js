var config = require("./config.json");
var path = require("path");
var fs = require("fs");

/* ------------------------------------------------------------------------ */
/* Data Store */
/* ------------------------------------------------------------------------ */

var Data = require("./modules/core-data/index.js");
console.log(Data);

Data.set("core", "modulesPath", "test");
console.log(Data);

/*
Data.core.modulesPath = path.join( __dirname, 'modules' );
console.log( Data.core.modulesPath );

Data.loadModules( Data.core.modulesPath );
*/

/* ------------------------------------------------------------------------ */
/* Populate Media Items on Reload */
/* ------------------------------------------------------------------------ */

var videoDir = config.video_directory;
var imagesDir = config.images_directory;

/* Get all media in directory matching prefix and suffix */
function listMedia(array, dir, prefix, ext) {
  fs.readdirSync(path.join(__dirname, dir)).forEach(file => {
    if (file.includes(prefix, 0) && file.includes(ext, ext.length * -1)) {
      array.push(file.replace(prefix, "").replace(ext, ""));
    }
  });
}

/* Setup available media for views */
var media = {};
media.trans = [];
media.background = [];
media.square = [];

/* Populate media object arrays */
listMedia(media.trans, videoDir, "trans-", ".webm");
listMedia(media.background, videoDir, "bg-", ".webm");
listMedia(media.square, imagesDir, "square-", ".png");

/* ------------------------------------------------------------------------ */
/* Websocket Connection */
/* ------------------------------------------------------------------------ */

var server = require("http").createServer(app);
var io = require("socket.io")(server);

/* ------------------------------------------------------------------------ */
/* Static Server and Websocket Connection */
/* ------------------------------------------------------------------------ */

var express = require("express");
var bodyParser = require("body-parser");
var app = express();

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.set("view engine", "pug");
app.set("views", path.join(__dirname, "views"));

/* Homepage */
app.use("/", express.static(path.join(__dirname, "public")));

/* Streaming Dashboard*/
app.get("/dashboard/", function(req, res) {
  /* Load data need for our dashboard view */
  var dashInfo = {
    secret: config.dashboard_secret,
    trans: media.trans,
    background: media.background,
    square: media.square,
    socket_port: config.proxy_socket_port,
    socket_host: config.proxy_socket_host
  };

  /* Require login */
  const auth = {
    login: config.dashboard_user,
    password: config.dashboard_password
  };
  const b64auth = (req.headers.authorization || "").split(" ")[1] || "";
  const [login, password] = new Buffer(b64auth, "base64").toString().split(":");

  if (
    !login ||
    !password ||
    login !== auth.login ||
    password !== auth.password
  ) {
    res.set("WWW-Authenticate", 'Basic realm="YBGamingStreamControls"');
    res.status(401).send("Username / Password Required");
    return;
  } else {
    /* Render the dashboard */
    res.render("dashboard", dashInfo);
  }
});

/* Pass Commands via Websocket */
app.post("/update/:command/", function(req, res) {
  /* If the command does not match the key (from outside source), ignore it */
  if (req.body.secret !== config.dashboard_secret) {
    res.status(404).render("404.pug");
    return;
  } else {
    delete req.body.secret;
  }

  /* If trans argument is passed trigger a transition via websocket */
  if (typeof req.body.trans !== "undefined") {
    io.emit("trans_play", { trans: req.body.trans });
  }

  /* Send our command and data via websocket */
  io.emit(req.params.command, req.body);
  res.send("Got it!");
});

/* Load stream layout view */
app.get("/:view/", function(req, res) {
  /* Load data data needed for overlay views */
  req.params.socket_port = config.proxy_socket_port;
  req.params.socket_host = config.proxy_socket_host;
  req.params.trans = media.trans;
  req.params.background = media.background;
  req.params.square = media.square;

  res.render(req.params.view, req.params);
});

/* Respond with 404 if not found */
app.use(function(err, req, res, next) {
  console.error(err.stack);
  res.status(404).render("404.pug");
});

/* ------------------------------------------------------------------------ */
/* Discord Bot */
/* ------------------------------------------------------------------------ */

var dctbot = require("./dctbot.js");

/* Setup default hype object */
var hype = {
  codeOne: "YB",
  codeTwo: "BR",
  max: 50,
  decay: 0.15,
  interval: 1000
};

/* Set Hype obj for Frontend JS */
app.post("/hype/", function(req, res) {
  /* If from valid dashboard, as above */
  if (req.body.secret !== config.dashboard_secret) {
    res.status(404).render("404.pug");
    return;
  } else {
    delete req.body.secret;
  }

  /* Update hype object with new properties */
  var props = ["codeOne", "codeTwo", "max", "decay", "interval"];

  for (var i = 0; i < props.length; i++) {
    if (typeof req.body[props[i]] !== "undefined") {
      hype[props[i]] = req.body[props[i]];
    }
  }

  /* Set hype meter settings for front end */
  io.emit("hype_set", hype);
  res.send("Got it!");
});

/* Overwrite onMessage hook with our code */
dctbot.onMessage = function onMessage(message) {
  console.log(message);

  /* If any message the bot processes contains the Hype codes, emit a hype +1 */
  if (message.includes(hype.codeOne)) {
    io.emit("hype_up_one", {});
  }
  if (message.includes(hype.codeTwo)) {
    io.emit("hype_up_two", {});
  }
};
/* Start the Discord bot with the token */
dctbot.start(config.discord_token);

/* ------------------------------------------------------------------------ */
/* Server Listen */
/* ------------------------------------------------------------------------ */

app.listen(config.http_port, config.http_host);
server.listen(config.socket_port, config.socket_host);
